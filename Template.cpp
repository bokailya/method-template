#include "Interface.hpp"
#include "Hero.hpp"
#include "Ability.hpp"

#include <memory>
#include <vector>

int main(void) {
	std::shared_ptr<Hero> mage(new Hero("Hello, mage!"));
	std::shared_ptr<Hero> goblin(new Hero("Hello, goblin!"));

	std::shared_ptr<Hero> heroes[] = {mage, goblin};

	std::shared_ptr<Ability> frostball(new Ability("Hello, frostball!"));
	std::shared_ptr<Ability> fireball(new Ability("Hello, fireball!"));

	std::vector<std::shared_ptr<Ability>> abilities({frostball, fireball});

	std::shared_ptr<Hero> returnedHero
		= Interface::print(heroes, heroes + 2);

	std::cout
		<< "Returned object said: "
		<< returnedHero->getInfo()
		<< std::endl;

	
	std::shared_ptr<Ability> returnedAbility
		= Interface::print(abilities.begin(), abilities.end());

	std::cout
		<< "Returned object said: "
		<< returnedAbility->getInfo()
		<< std::endl;

	return 0;
}
