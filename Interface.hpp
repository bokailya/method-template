#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include <iostream>

class Interface {
	public:
		template <typename Iter, typename T = typename std::iterator_traits<Iter>::value_type>
		static T print(Iter begin, Iter end);
};

template <typename Iter, typename T>
T Interface::print(Iter begin, Iter end) {
	for (Iter it = begin; it != end; it++)
		std::cout << (*it)->getInfo() << std::endl;
	return *begin;
}

#endif // INTERFACE_HPP
