#ifndef HERO_HPP
#define HERO_HPP

#include <string>

class Hero {
	public:
		Hero(const std::string& info);
		const std::string &getInfo(void);
	private:
		std::string _info;
};

#endif // HERO_HPP
