LINK.o = $(LINK.cc)
CXX = g++
CXXFLAGS=-Wall -Wextra --std=c++0x

Template: Ability.o Hero.o Template.o

Ability.o: Ability.cpp Ability.hpp
Hero.o: Hero.cpp Hero.hpp
Template.o: Template.cpp  Ability.hpp Hero.hpp Interface.hpp
