#ifndef ABILITY_HPP
#define ABILITY_HPP

#include <string>

class Ability {
	public:
		Ability(const std::string& info);
		const std::string &getInfo(void);
	private:
		std::string _info;
};

#endif // ABILITY_HPP
