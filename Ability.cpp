#include "Ability.hpp"

Ability::Ability(const std::string& info):_info(info) {
}

const std::string &Ability::getInfo(void) {
	return _info;
}
